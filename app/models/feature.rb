class Feature

	attr_accessor :clauses, :client_ip, :ua_name, :ua_version, :ua_os, :username
  attr_accessor :country_code, :groups

	def initialize(clauses, client_ip: nil, 
		                    ua_name: nil, ua_version: nil, ua_os: nil, 
		                    username: nil)
	  super()
	  self.clauses = clauses
	  self.client_ip = client_ip
	  self.ua_name = ua_name
	  self.ua_version = ua_version
	  self.ua_os = ua_os
	  self.username = username
    self.country_code = nil
    self.groups = nil
	end


	def evaluate
	  return true if clauses == {}
	  return clauses unless clauses.is_a? Hash
	  r = {}
	  clauses.each do |cond, data|
      case cond
      when "allow_ips"
        return false unless client_ip.present?
        data = ip_matcher(data).any?
      when "disallow_ips"
        return false unless client_ip.present?
        data = ip_matcher(data).none?
      when "allow_user_agents"
        return false unless ua_name.present?
        data = ua_name_matcher(data).any?
      when "disallow_user_agents"
        return false unless ua_name.present?
        data = ua_name_matcher(data).none?
      when "allow_countries"
        return false unless client_ip.present?
        data = country_matcher(data).any?
      when "disallow_countries"
        return false unless client_ip.present?
        data = country_matcher(data).none?
      when "allow_groups"
        return false unless username.present?
        data = group_matcher(data).any?
      when "disallow_groups"
        return false unless username.present?
        data = group_matcher(data).none?
      end
      r[cond] = data
	  end
    # At this point, if all clauses are true, just return true. (= show this feature)
    return true if r.all? { |k,v| v == true }
    # If any clause is false, return false (= don't show this feature, condition failed)
    return false if r.any? { |k,v| v == false }
    # Otherwise, return only those clauses which are hashes (= run-time decision for the client)
    r.select { |k,v| v.is_a? Hash }
	end	


	def ip_matcher(ips)
	  ips = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"] if ips == "private"
    ips = [ips] unless ips.is_a? Array

    ips.map do |ip| 
      if ip.include?("/")
        NetAddr::CIDR.create(ip).matches?(client_ip)
      else
        ip == client_ip
      end
    end
	end


	def ua_name_matcher(data)
    data = [data] unless data.is_a? Array
    data = data.map do |x| 
      if x.is_a? Array
        if x[1].present? && x[2].present?
           return [false] unless ua_version
           v = x[2].to_f
           uav = ua_version.to_f
           ua_name == x[0] && case x[1]
                              when '>' then uav > v
                              when '>=' then uav >= v
                              when '=' then uav == v
                              when '!=' then uav != v
                              when '<=' then uav <= v
                              when '<' then uav < v
                              end
        else
          ua_name == x[0]
        end
      else
        ua_name == x
      end
    end
	end


  def country_matcher(data)
    data = [data] unless data.is_a? Array
    data.map do |cc| 
      self.country_code ||= lookup_country
      country_code == cc
    end
  end


  def lookup_country
    return nil if client_ip.blank?
    begin
      response = Api.request("http://freegeoip.net/json/#{client_ip}", :get)
    rescue Api::TimeoutError, Api::NoResponseError
      return nil
    end
    return nil unless response.status == 200
    begin
      response.body['country_code']
    rescue JSON::ParserError
      nil
    end
  end


  def group_matcher(data)
    data = [data] unless data.is_a? Array
    data.map do |group_name| 
      self.groups ||= lookup_groups(username)
      groups.include? group_name
    end
  end


  def lookup_groups(username)
    api_users = Api::RemoteResource.get!("/v1/api_users?username=#{username}")
    u = api_users.collection.first
    return [] unless u
    groups = u.get! :groups
    groups.collection.map { |g| g['name'] }
  rescue Api::TimeoutError, Api::NoResponseError, 
         Api::RemoteResource::GetFailed, Api::RemoteResource::UnparseableJson
    []
  end

end
