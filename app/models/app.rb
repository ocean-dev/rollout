class App < OceanDynamo::Table

  dynamo_schema :id, table_name_suffix: Api.basename_suffix,
                     create: true, client: $dynamo_client do
    attribute :name
    attribute :description
  	attribute :created_by
  	attribute :updated_by
    attribute :features,    :serialized, default: {}
  end

  ocean_resource_model index: [:name], search: false


  # Attributes


  # Validations
  validates :name, presence: true
  validates :name, format: { with: /\A(\*|[A-Za-z0-9_-]+)\z/,
              message: "may only contain the characters A-Z, a-z, 0-9, underscore and hyphen" }
  validate do
    if persisted? && name != id
      errors.add(:name, "Changing the name of an app is not permitted")
    end
  end


  # Callbacks
  before_create do |app|
    app.id = app.name
    raise OceanDynamo::RecordNotUnique if App.find_by_id app.id
  end


  def activated_features(client_ip: nil, ua_name: nil, ua_version: nil, ua_os: nil, username: nil)
    result = {}
    features.collect do |feature, clauses|
      f = Feature.new clauses, client_ip: client_ip,
                               ua_name: ua_name, ua_version: ua_version, ua_os: ua_os,
                               username: username
      result[feature] = f.evaluate
    end
    result
  end

end
