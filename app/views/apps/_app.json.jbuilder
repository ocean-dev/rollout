json.app do |json|
	json._links       hyperlinks(self:               app_url(app),
	                             activated_features: activated_features_app_url(app),
	                             creator:            smart_api_user_url(app.created_by),
	                             updater:            smart_api_user_url(app.updated_by))
	json.(app, :lock_version,
	           :name,
	           :description, 
	           :features) 
	json.created_at   app.created_at.utc.iso8601
	json.updated_at   app.updated_at.utc.iso8601
end
