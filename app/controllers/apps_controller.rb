class AppsController < ApplicationController

  ocean_resource_controller required_attributes: [:lock_version],
                            extra_actions: {'activated_features' => ["activated_features", "GET"]}

  #respond_to :json
  
  before_action :find_app, except: [:create, :index]

  # POST /apps
  def create
    @app = App.new(params)
    set_updater(@app)
    @app.save!
    api_render @app, new: true
  end


  # GET /apps/foo
  def show
    expires_in 0, 's-maxage' => 1.day
    if stale?(etag: @app.lock_version,          # NB: DynamoDB tables dont have cache_key - FIX!
              last_modified: @app.updated_at)
      api_render @app
    end
  end


  # GET /apps
  def index
    expires_in 0, 's-maxage' => 1.day
    # if stale?(collection_etag(App))   # collection_etag is still ActiveRecord only!
    # Instead, we get all the Apps (they are few) and compute the ETag manually:
    @apps = App.all    
    latest = @apps.max_by(&:updated_at)
    last_updated = latest && latest.updated_at 
    if stale?(etag: "App:#{App.count}:#{last_updated}")
      api_render @apps
    end
  end


  # PUT /apps/foo
  def update
    @app.assign_attributes name:         params[:name], 
    	                     description:  params[:description],
    	                     features:     params[:features],
    	                     lock_version: params[:lock_version]
    set_updater(@app)
    @app.save!
    api_render @app
  end


  # DELETE /apps/foo
  def destroy
    @app.destroy
    render_head_204
  end


  # GET /apps/foo/activated_features
  def activated_features
    expires_in 0, 's-maxage' => 1.day
    if stale?(etag: @app.lock_version,          # NB: DynamoDB tables dont have cache_key - FIX!
              last_modified: @app.updated_at)
      render json: @app.activated_features(client_ip:  params['client-ip'], 
                                           username:   params[:username],
                                           ua_name:    params['ua-name'],
                                           ua_version: params['ua-version'],
                                           ua_os:      params['ua-os']
                                          )
    end
  end


  private
     
  def find_app
    @app = App.find_by_id params[:id]
    return true if @app
    render_api_error 404, "App not found"
    false
  end

end
