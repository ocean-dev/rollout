#
# This task makes sure the DB has all Apps needed.
# This script is idempotent.
#
# For this reason, this task should be run as part of deployment, whether
# by TeamCity agents or of environments such as master, staging, and prod.
#
require 'pp'

namespace :ocean do
  
  desc "Updates the client Apps"
  task :update_apps => :environment do

    require 'app'
 
    puts
    puts "============================================================"
    puts "Processing Apps..."
   
    f = File.join(Rails.root, "config/app_data.json")
    basic_set = JSON.parse(File.read(f))
    puts "The number of Apps to process is #{basic_set.length}"

    basic_set.each { |a| update_app(a) }
    puts
    puts "Done."
  end


  def update_app(a)
    puts
    app = App.find_by_id a['name']
    if app
      puts "Updating app #{a['name']}."
      app.send(:update_attributes, a)
    else
      puts "Creating app #{a['name']}."
      app = App.create! a
    end
  end

end


