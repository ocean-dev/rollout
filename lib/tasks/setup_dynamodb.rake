#
# Sets up the DynamoDB table
#

namespace :ocean do
  desc 'Connects to the DynamoDB tables, effectively creating them if not already created'
  task setup_dynamodb: :environment do
    require 'app'

    puts "============================================================"
    puts "Setting up App..."
    App.establish_db_connection
    puts "Done."

    puts "All done."

  end
end
