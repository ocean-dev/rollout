namespace :ocean do

  desc 'Sets up and/or updates the app definitions.'
  task setup_all: :environment do
    Rake::Task["ocean:setup_dynamodb"].invoke
    Rake::Task["ocean:update_apps"].invoke
  end
end
