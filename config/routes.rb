# == Route Map
#
#                 Prefix Verb   URI Pattern                               Controller#Action
# activated_features_app GET    /v1/apps/:id/activated_features(.:format) apps#activated_features {:id=>/[A-Za-z0-9_-]+/}
#                   apps GET    /v1/apps(.:format)                        apps#index
#                        POST   /v1/apps(.:format)                        apps#create
#                    app GET    /v1/apps/:id(.:format)                    apps#show {:id=>/[A-Za-z0-9_-]+/}
#                        PATCH  /v1/apps/:id(.:format)                    apps#update {:id=>/[A-Za-z0-9_-]+/}
#                        PUT    /v1/apps/:id(.:format)                    apps#update {:id=>/[A-Za-z0-9_-]+/}
#                        DELETE /v1/apps/:id(.:format)                    apps#destroy {:id=>/[A-Za-z0-9_-]+/}
#                  alive GET    /alive(.:format)                          alive#index
#

Rollout::Application.routes.draw do

  scope "v1" do
  	# Put resource routes here

  	resources :apps, only: [:index, :create, :show, :update, :destroy], 
                           constraints: {id: /[A-Za-z0-9_-]+/} do
      member do
      	get "activated_features"
      end
    end


  end

end
