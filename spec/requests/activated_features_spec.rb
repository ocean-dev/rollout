require 'spec_helper'

describe App, :type => :request do

  before :each do
    App.delete_all
    permit_with 200
  end
  


  describe "activated_features" do

    before :each do
      @app = create :app, 
               name: "travel_client",
               features: { 
                  "hotel_flow" => { 
                    "allow_ips" => "private",
                    "disallow_user_agents" => [["IE", "<", 10.0]],
                    "allow_countries" => "SE",
                    "custom" => {"random" => 0.1}
                  }
               }
    end

    it "should allow Chrome from our private networks" do
      allow_any_instance_of(Feature).to receive(:lookup_country).and_return "SE"
      get "/v1/apps/travel_client/activated_features?client-ip=10.11.12.13&ua-name=Chrome&ua-version=10.1", {},
            {'HTTP_ACCEPT' => "application/json", 'X-API-TOKEN' => "fake"}
      expect(response.status).to eq 200
      expect(response.body).to eq "{\"hotel_flow\":{\"custom\":{\"random\":0.1}}}"
    end

  end

end 
