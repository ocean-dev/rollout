# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :app do
  	name { SecureRandom.urlsafe_base64 }
  	description "This is a great Ocean app."
    created_by "https://api.example.com/v1/api_users/123"
    updated_by "https://api.example.com/v1/api_users/123"
  end
end
