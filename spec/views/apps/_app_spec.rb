require 'spec_helper'

describe "apps/_app", :type => :view do
  
  before :all do 
    App.destroy_all
  end

  before :each do                     # Must be :each (:all causes all tests to fail)
    render partial: "apps/app", locals: {app: create(:app, features: {"foo" => true, 
                                                                       "bar" => false})}
    @json = JSON.parse(rendered)
    @u = @json['app']
    @links = @u['_links'] rescue {}
  end

  after :all do 
    App.destroy_all
  end


  it "has a named root" do
    expect(@u).not_to eq nil
  end


  it "should have four hyperlinks" do
    expect(@links.size).to eq 4
  end

  it "should have a self hyperlink" do
    expect(@links).to be_hyperlinked('self', /apps/)
  end

  it "should have an activated_features hyperlink" do
    expect(@links).to be_hyperlinked('activated_features', /apps/)
  end

  it "should have a creator hyperlink" do
    expect(@links).to be_hyperlinked('creator', /api_users/)
  end

  it "should have an updater hyperlink" do
    expect(@links).to be_hyperlinked('updater', /api_users/)
  end


  it "should have a name field" do
    expect(@u['name']).to be_a String
  end

  it "should have a description field" do
    expect(@u['description']).to be_a String
  end

  it "should have a features field" do
    expect(@u['features']).to eq({"foo" => true, "bar" => false})
  end


  it "should have a created_at time" do
    expect(@u['created_at']).to be_a String
  end

  it "should have an updated_at time" do
    expect(@u['updated_at']).to be_a String
  end

  it "should have a lock_version field" do
    expect(@u['lock_version']).to be_an Integer
  end
      
end
