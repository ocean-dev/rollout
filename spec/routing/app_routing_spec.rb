require "spec_helper"

describe AppsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(get("/v1/apps")).to route_to("apps#index")
    end

    it "routes to #show" do
      expect(get("/v1/apps/foo")).to route_to("apps#show", :id => "foo")
    end

    it "routes to #create" do
      expect(post("/v1/apps")).to route_to("apps#create")
    end

    it "routes to #update" do
      expect(put("/v1/apps/foo")).to route_to("apps#update", :id => "foo")
    end

    it "routes to #destroy" do
      expect(delete("/v1/apps/foo")).to route_to("apps#destroy", :id => "foo")
    end

    it "routes to #activated_features" do
      expect(get("/v1/apps/foo/activated_features")).to route_to("apps#activated_features", :id => "foo")
    end
  end
end
