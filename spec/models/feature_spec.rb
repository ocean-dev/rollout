require 'spec_helper'

describe Feature, :type => :model do

  before :each do
    App.destroy_all
  end


  it "should require a hash of clauses" do
  	expect(Feature.new({})).to be_a Feature
  end	

  it "should accept the standard client data keywords" do
  	expect(Feature.new({}, client_ip: nil, ua_name: nil, ua_version: nil, ua_os: nil, username: nil)).
  	  to be_a Feature
  end

  it "should have a #clauses accessor" do
  	expect(Feature.new({}).clauses).to eq({})
  end

  it "should have a #client_ip accessor" do
  	expect(Feature.new({}, client_ip: "1.2.3.4").client_ip).to eq "1.2.3.4"
  end

  it "should have an #ua_name accessor" do
  	expect(Feature.new({}, ua_name: "Firefox").ua_name).to eq "Firefox"
  end

  it "should have an #ua_version accessor" do
  	expect(Feature.new({}, ua_version: "10.2").ua_version).to eq "10.2"
  end

  it "should have an #ua_os accessor" do
  	expect(Feature.new({}, ua_os: "Windows Vista").ua_os).to eq "Windows Vista"
  end

  it "should have a #username accessor" do
    expect(Feature.new({}, username: "beelzebub").username).to eq "beelzebub"
  end

  it "should have a #country_code accessor" do
    expect(Feature.new({})).to respond_to :country_code
  end

  it "should have a #groups accessor" do
    expect(Feature.new({})).to respond_to :groups
  end


  describe "evaluate" do

    it "should return feature => true when the clauses are {}" do
      expect(Feature.new({}).evaluate).to eq(true)
    end

    it "should not remove the feature when the clauses are false" do
      expect(Feature.new(false).evaluate).to eq(false)
    end


    describe "allow_ips" do

      it "should be false when there is no client_ip" do
        expect(Feature.new({'allow_ips' => '2.3.4.5'}, client_ip: nil).evaluate).
          to eq false
      end

      it "should return true if there is a matching IP" do
        expect(Feature.new({'allow_ips' => '2.3.4.5'}, client_ip: "2.3.4.5").evaluate).
          to eq true
      end

      it "should return false if there is no matching IP" do
        expect(Feature.new({'allow_ips' => '2.3.4.5'}, client_ip: "10.0.0.0").evaluate).
          to eq false
      end

      it "should return true if there is a matching CIDR" do
        expect(Feature.new({'allow_ips' => '10.0.0.0/8'}, client_ip: "10.3.4.5").evaluate).
          to eq true
      end

      it "should return false if there is no matching CIDR" do
        expect(Feature.new({'allow_ips' => '10.0.0.0/16'}, client_ip: "10.3.4.5").evaluate).
          to eq false
      end

      it "should match an array, treating it as an OR operation" do
        expect(Feature.new({'allow_ips' => ['10.0.0.0/8', '127.0.0.1']}, client_ip: "127.0.0.1").evaluate).
          to eq true
      end

      it "should treat the string 'private' as short for the standard private IP ranges" do
        expect(Feature.new({'allow_ips' => "private"}, client_ip: "172.20.255.255").evaluate).
          to eq true
      end
    end


    describe "disallow_ips" do

      it "should be false when there is no client_ip" do
        expect(Feature.new({'disallow_ips' => '2.3.4.5'}, client_ip: nil).evaluate).
          to eq false
      end

      it "should return false if there is a matching IP" do
        expect(Feature.new({'disallow_ips' => '2.3.4.5'}, client_ip: "2.3.4.5").evaluate).
          to eq false
      end

      it "should return true if there is no matching IP" do
        expect(Feature.new({'disallow_ips' => '2.3.4.5'}, client_ip: "10.0.0.0").evaluate).
          to eq true
      end

      it "should return false if there is a matching CIDR" do
        expect(Feature.new({'disallow_ips' => '10.0.0.0/8'}, client_ip: "10.3.4.5").evaluate).
          to eq false
      end

      it "should return true if there is no matching CIDR" do
        expect(Feature.new({'disallow_ips' => '10.0.0.0/16'}, client_ip: "10.3.4.5").evaluate).
          to eq true
      end

      it "should match an array, treating it as an OR operation" do
        expect(Feature.new({'disallow_ips' => ['10.0.0.0/8', '127.0.0.1']}, client_ip: "127.0.0.1").evaluate).
          to eq false
      end

      it "should treat the string 'private' as short for the standard private IP ranges" do
        expect(Feature.new({'disallow_ips' => "private"}, client_ip: "172.20.255.255").evaluate).
          to eq false
      end
    end


    describe "allow_user_agents" do

      it "should be false when there is no ua_name" do
        expect(Feature.new({'allow_user_agents' => 'Chrome'}, ua_name: nil).evaluate).
          to eq false
      end

      it "should return true if there is a matching ua_name" do
        expect(Feature.new({'allow_user_agents' => 'Chrome'}, ua_name: "Chrome").evaluate).
          to eq true
      end

      it "should return false if there is no matching ua_name" do
        expect(Feature.new({'allow_user_agents' => 'Chrome'}, ua_name: "Firefox").evaluate).
          to eq false
      end

      it "should match an array, treating it as an OR operation" do
        expect(Feature.new({'allow_user_agents' => ['Chrome', 'Safari']}, ua_name: "Safari").evaluate).
          to eq true
      end

      describe "with comparator list" do
        it "should use the first element as the Agent name" do
          expect(Feature.new({'allow_user_agents' => [['Chrome']]}, ua_name: "Chrome").evaluate).
            to eq true
        end

        it "should use the second element as the comparator and the third as the value" do
          expect(Feature.new({'allow_user_agents' => [['IE', '>=', 10]]}, 
          	               ua_name: 'IE', ua_version: '9.50').evaluate).
            to eq false
          expect(Feature.new({'allow_user_agents' => [['IE', '>=', 10]]}, 
                           ua_name: 'IE', ua_version: '11').evaluate).
            to eq true
          expect(Feature.new({'allow_user_agents' => [['IE', '>=', 10]]}, 
                           ua_name: 'IE', ua_version: '8').evaluate).
            to eq false
          expect(Feature.new({'allow_user_agents' => ["Chrome", ['IE', '>=', 10]]}, 
          	               ua_name: 'Chrome', ua_version: '2').evaluate).
            to eq true
        end
      end
    end


    describe "disallow_user_agents" do

      it "should be false when there is no ua_name" do
        expect(Feature.new({'disallow_user_agents' => 'Chrome'}, ua_name: nil).evaluate).
          to eq false
      end

      it "should return false if there is a matching ua_name" do
        expect(Feature.new({'disallow_user_agents' => 'Chrome'}, ua_name: "Chrome").evaluate).
          to eq false
      end

      it "should return true if there is no matching ua_name" do
        expect(Feature.new({'disallow_user_agents' => 'Chrome'}, ua_name: "Firefox").evaluate).
          to eq true
      end

      it "should match an array, treating it as an OR operation" do
        expect(Feature.new({'disallow_user_agents' => ['Chrome', 'Safari']}, ua_name: "Safari").evaluate).
          to eq false
      end

      describe "with comparator list" do
        it "should use the first element as the Agent name" do
          expect(Feature.new({'disallow_user_agents' => [['Chrome']]}, ua_name: "Chrome").evaluate).
            to eq false
        end

        it "should use the second element as the comparator and the third as the value" do
          expect(Feature.new({'disallow_user_agents' => [['IE', '>=', 10]]}, 
          	               ua_name: 'IE', ua_version: '9.50').evaluate).
            to eq true
          expect(Feature.new({'disallow_user_agents' => [['IE', '>=', 10]]}, 
                           ua_name: 'IE', ua_version: '11').evaluate).
            to eq false
          expect(Feature.new({'disallow_user_agents' => [['IE', '>=', 10]]}, 
                           ua_name: 'IE', ua_version: '8').evaluate).
            to eq true
          expect(Feature.new({'disallow_user_agents' => ["Chrome", ['IE', '>=', 10]]}, 
          	               ua_name: 'Chrome', ua_version: '2').evaluate).
            to eq false
        end
      end
    end


    describe "allow_countries" do

      it "should be false when there is no client_ip" do
        expect_any_instance_of(Feature).not_to receive :lookup_country
        expect(Feature.new({'allow_countries' => 'SE'}, client_ip: nil).evaluate).
          to eq false
      end

      it "should return true if there is a matching country" do
        expect_any_instance_of(Feature).to receive(:lookup_country).once.and_return "SE"
        expect(Feature.new({'allow_countries' => 'SE'}, client_ip: "1.2.3.4").evaluate).
          to eq true
      end

      it "should return false if there is no matching country" do
        expect_any_instance_of(Feature).to receive(:lookup_country).once.and_return "NO"
        expect(Feature.new({'allow_countries' => 'SE'}, client_ip: "1.2.3.4").evaluate).
          to eq false
      end

      it "should match an array, treating it as an OR operation" do
        expect_any_instance_of(Feature).to receive(:lookup_country).once.and_return "SE"
        expect(Feature.new({'allow_countries' => ['GB', 'SE']}, client_ip: "1.2.3.4").evaluate).
          to eq true
      end
    end


    describe "disallow_countries" do

      it "should be false when there is no client_ip" do
        expect_any_instance_of(Feature).not_to receive :lookup_country
        expect(Feature.new({'disallow_countries' => 'SE'}, client_ip: nil).evaluate).
          to eq false
      end

      it "should return false if there is a matching country" do
        expect_any_instance_of(Feature).to receive(:lookup_country).once.and_return "SE"
        expect(Feature.new({'disallow_countries' => 'SE'}, client_ip: "1.2.3.4").evaluate).
          to eq false
      end

      it "should return true if there is no matching country" do
        expect_any_instance_of(Feature).to receive(:lookup_country).once.and_return "NO"
        expect(Feature.new({'disallow_countries' => 'SE'}, client_ip: "1.2.3.4").evaluate).
          to eq true
      end

      it "should match an array, treating it as an OR operation" do
        expect_any_instance_of(Feature).to receive(:lookup_country).once.and_return "SE"
        expect(Feature.new({'disallow_countries' => ['GB', 'SE']}, client_ip: "1.2.3.4").evaluate).
          to eq false
      end
    end


    describe "allow_groups" do

      it "should be false when there is no username" do
        expect(Feature.new({'allow_groups' => 'Product Owners'}, username: nil).evaluate).
          to eq false
      end

      it "should return true if the user belongs to the group" do
        expect_any_instance_of(Feature).to receive(:lookup_groups).once.with("tom").
          and_return ["Developers", "Product Owners"]
        expect(Feature.new({'allow_groups' => 'Product Owners'}, username: "tom").evaluate).
          to eq true
      end

      it "should return false if user doesn't belong to the group" do
        expect_any_instance_of(Feature).to receive(:lookup_groups).once.with("tom").
          and_return ["Foos", "Bars"]
        expect(Feature.new({'allow_groups' => 'Product Owners'}, username: "tom").evaluate).
          to eq false
      end

      it "should match an array, treating it as an OR operation" do
        expect_any_instance_of(Feature).to receive(:lookup_groups).once.with("tom").
          and_return ["Developers", "Product Owners"]
        expect(Feature.new({'allow_groups' => ['Beta', 'Developers']}, username: "tom").evaluate).
          to eq true
      end
    end


    describe "disallow_groups" do

      it "should be false when there is no username" do
        expect(Feature.new({'disallow_groups' => 'Product Owners'}, username: nil).evaluate).
          to eq false
      end

      it "should return true if the user doesn't belong to the group" do
        expect_any_instance_of(Feature).to receive(:lookup_groups).once.with("tom").
          and_return ["Developers", "Product Owners"]
        expect(Feature.new({'disallow_groups' => 'Beta'}, username: "tom").evaluate).
          to eq true
      end

      it "should return false if user belongs to the group" do
        expect_any_instance_of(Feature).to receive(:lookup_groups).once.with("tom").
          and_return ["Foos", "Bars"]
        expect(Feature.new({'disallow_groups' => 'Bars'}, username: "tom").evaluate).
          to eq false
      end

      it "should match an array, treating it as an OR operation" do
        expect_any_instance_of(Feature).to receive(:lookup_groups).once.with("tom").
          and_return ["Developers", "Product Owners"]
        expect(Feature.new({'disallow_groups' => ['Beta', 'Foos']}, username: "tom").evaluate).
          to eq true
      end
    end


  end


  describe "lookup_country" do

    it "should make a call to http://freegeoip.net" do
     stub_request(:get, "http://freegeoip.net/json/1.2.3.4").
         with(:headers => {'Accept'=>'application/json', 'User-Agent'=>'Ocean'}).
         to_return(status: 200, body: '{"country_code":"SE"}', headers: {})      
      expect(Feature.new({}, client_ip: "1.2.3.4").lookup_country).to eq "SE"
    end

    it "should handle timeouts gracefully" do
      stub_request(:get, "http://freegeoip.net/json/1.2.3.4").
         with(:headers => {'Accept'=>'application/json', 'User-Agent'=>'Ocean'}).
         to_timeout      
      expect(Feature.new({}, client_ip: "1.2.3.4").lookup_country).to eq nil
    end

    it "should handle no response gracefully" do
      stub_request(:get, "http://freegeoip.net/json/1.2.3.4").
         with(:headers => {'Accept'=>'application/json', 'User-Agent'=>'Ocean'}).
         to_return(status: 0, body: nil, headers: nil)      
      expect(Feature.new({}, client_ip: "1.2.3.4").lookup_country).to eq nil
    end

    it "should handle errors gracefully" do
     stub_request(:get, "http://freegeoip.net/json/1.2.3.4").
         with(:headers => {'Accept'=>'application/json', 'User-Agent'=>'Ocean'}).
         to_return(status: 404, body: 'Not Found', headers: {})      
      expect(Feature.new({}, client_ip: "1.2.3.4").lookup_country).to eq nil
    end

    it "should handle non-JSON bodies gracefully" do
     stub_request(:get, "http://freegeoip.net/json/1.2.3.4").
         with(:headers => {'Accept'=>'application/json', 'User-Agent'=>'Ocean'}).
         to_return(status: 200, body: 'Way out, man!', headers: {})      
      expect(Feature.new({}, client_ip: "1.2.3.4").lookup_country).to eq nil
    end
  end


  describe "lookup_groups" do

    before :each do 
      WebMock.disable_net_connect! 
      allow(Api).to receive(:service_token).and_return "the-token"
      allow(Object).to receive(:sleep)
      @api_users_body = '{"_collection":{"resources":[{"api_user":{"username":"pjotr","real_name":"Peter Bengtson","email":"peter.bengtson@odigeo.com","authentication_duration":1800,"login_blocked":false,"created_at":"2014-01-06T12:52:01Z","updated_at":"2014-09-15T19:16:51Z","lock_version":7,"_links":{"self":{"href":"https://master-api.odigeoservices.com/v1/api_users/17","type":"application/json"},"authentications":{"href":"https://master-api.odigeoservices.com/v1/api_users/17/authentications","type":"application/json"},"rights":{"href":"https://master-api.odigeoservices.com/v1/api_users/17/rights","type":"application/json"},"roles":{"href":"https://master-api.odigeoservices.com/v1/api_users/17/roles","type":"application/json"},"groups":{"href":"https://master-api.odigeoservices.com/v1/api_users/17/groups","type":"application/json"},"connect":{"href":"https://master-api.odigeoservices.com/v1/api_users/17/connect","type":"application/json"},"creator":{"href":"https://master-api.odigeoservices.com/v1/api_users/8","type":"application/json"},"updater":{"href":"https://master-api.odigeoservices.com/v1/api_users/17","type":"application/json"}}}}],"count":1,"total_count":1,"_links":{"self":{"href":"https://master-api.odigeoservices.com/v1/api_users?_right_restrictions%5B%5D%5Bapp%5D=%2A\u0026_right_restrictions%5B%5D%5Bcontext%5D=%2A\u0026username=pjotr","type":"application/json"}}}}'
      @groups = '{"_collection":{"resources":[{"group":{"name":"Developers","description":"The group of developers.","lock_version":20,"created_at":"2014-01-06T14:51:48Z","updated_at":"2014-10-10T00:01:36Z","_links":{"self":{"href":"https://master-api.odigeoservices.com/v1/groups/5","type":"application/json"},"documentation":{"href":"http://wiki.oceanframework.net/index.php/Core_Groups#Developers","type":"text/html"},"api_users":{"href":"https://master-api.odigeoservices.com/v1/groups/5/api_users","type":"application/json"},"roles":{"href":"https://master-api.odigeoservices.com/v1/groups/5/roles","type":"application/json"},"rights":{"href":"https://master-api.odigeoservices.com/v1/groups/5/rights","type":"application/json"},"connect":{"href":"https://master-api.odigeoservices.com/v1/groups/5/connect","type":"application/json"},"creator":{"href":"https://master-api.odigeoservices.com/v1/api_users/8","type":"application/json"},"updater":{"href":"https://master-api.odigeoservices.com/v1/api_users/8","type":"application/json"}}}},{"group":{"name":"Superusers","description":"The Superuser group. Superusers have the same rights as the God role.","lock_version":28,"created_at":"2014-01-06T14:50:48Z","updated_at":"2014-10-17T14:30:13Z","indestructible":true,"_links":{"self":{"href":"https://master-api.odigeoservices.com/v1/groups/4","type":"application/json"},"documentation":{"href":"http://wiki.oceanframework.net/index.php/Core_Groups#Superusers","type":"text/html"},"api_users":{"href":"https://master-api.odigeoservices.com/v1/groups/4/api_users","type":"application/json"},"roles":{"href":"https://master-api.odigeoservices.com/v1/groups/4/roles","type":"application/json"},"rights":{"href":"https://master-api.odigeoservices.com/v1/groups/4/rights","type":"application/json"},"connect":{"href":"https://master-api.odigeoservices.com/v1/groups/4/connect","type":"application/json"},"creator":{"href":"https://master-api.odigeoservices.com/v1/api_users/8","type":"application/json"},"updater":{"href":"https://master-api.odigeoservices.com/v1/api_users/8","type":"application/json"}}}}],"count":2,"total_count":2,"_links":{"self":{"href":"https://master-api.odigeoservices.com/v1/api_users/17/groups?_right_restrictions%5B%5D%5Bapp%5D=%2A\u0026_right_restrictions%5B%5D%5Bcontext%5D=%2A","type":"application/json"}}}}'
    end
    
    after :each do 
      WebMock.allow_net_connect!   
    end

    it "should call the ApiUser service to obtain the user's groups" do
      stub_request(:get, "#{INTERNAL_OCEAN_API_URL}/v1/api_users?username=harry").
         to_return(:status => 200, :body => @api_users_body)
      stub_request(:get, "https://master-api.odigeoservices.com/v1/api_users/17/groups").
         to_return(:status => 200, :body => @groups)
      expect(Feature.new({}).lookup_groups("harry")).to eq ["Developers", "Superusers"]
    end

    it "should handle timeouts gracefully" do
      stub_request(:get, "#{INTERNAL_OCEAN_API_URL}/v1/api_users?username=harry").
        to_timeout
      expect(Feature.new({}).lookup_groups("harry")).to eq []
    end

    it "should handle no response gracefully" do
      stub_request(:get, "#{INTERNAL_OCEAN_API_URL}/v1/api_users?username=harry").
         to_return(:status => 200, :body => @api_users_body)
      stub_request(:get, "https://master-api.odigeoservices.com/v1/api_users/17/groups").
        to_return(:status => 0)
      expect(Feature.new({}).lookup_groups("harry")).to eq []
    end

    it "should handle nonexistent ApiUsers gracefully" do
      notfound = '{"_collection":{"resources":[],"count":0,"total_count":0,"_links":{"self":{"href":"https://master-api.odigeoservices.com/v1/api_users?_right_restrictions%5B%5D%5Bapp%5D=%2A\u0026_right_restrictions%5B%5D%5Bcontext%5D=%2A\u0026username=pjotr","type":"application/json"}}}}'
      stub_request(:get, "#{INTERNAL_OCEAN_API_URL}/v1/api_users?username=harry").
         to_return(:status => 200, :body => notfound)
      expect(Feature.new({}).lookup_groups("harry")).to eq []
    end

    it "should handle 500s gracefully" do
      stub_request(:get, "#{INTERNAL_OCEAN_API_URL}/v1/api_users?username=harry").
        to_return(:status => 503)
      expect(Feature.new({}).lookup_groups("harry")).to eq []
    end

    it "should handle non-JSON bodies gracefully" do
      stub_request(:get, "#{INTERNAL_OCEAN_API_URL}/v1/api_users?username=harry").
        to_return(:status => 200, body: "so not json")
      expect(Feature.new({}).lookup_groups("harry")).to eq []
    end

  end

end
