require 'spec_helper'

describe App, :type => :model do

  before :each do
    App.destroy_all
  end


  describe "attributes" do

    it "should have an id" do
      expect(create(:app).id).to be_a String
    end

    it "should have a name" do
      expect(create(:app, name: "foo").name).to eq "foo"
    end

    it "should require a name" do
      expect(build(:app, name: nil)).not_to be_valid
    end

    it "should only allow A-Za-Z0-9_- in name" do
      expect(build :app, name: "not good").not_to be_valid
      expect(build :app, name: "VeryNice_Oooh-2").to be_valid
    end

    it "should have a description" do
      expect(create(:app).description).to be_a String
    end
    
    it "should have a creation time" do
      expect(create(:app).created_at).to be_a Time
    end

    it "should have an update time" do
      expect(create(:app).updated_at).to be_a Time
    end
  
    it "should have a creator" do
      expect(create(:app).created_by).to be_a String
    end

    it "should have an updater" do
      expect(create(:app).updated_by).to be_a String
    end

    it "should include a features attribute" do
      expect(create(:app)).to respond_to :features
    end

    it "should serialise the features attribute" do
      expect(create(:app, features: {"foo" => true}).features).to eq({"foo"=>true})
    end
  end


  describe "instantiation" do

    it "should set the id to the name before creation" do
      app = build :app, name: "schmoo"
      expect(app.name).to eq "schmoo"
      expect(app.id).to eq ""
      app.save!
      expect(app.id).to eq "schmoo"
    end

    it "should not allow the name or id to be changed after creation" do
      app = create :app, name: "some_client"
      app.name = "Ryvita"
      expect(app).not_to be_valid
    end

    it "should require the name/id to be unique" do
      app = create :app, name: "blahonga"
      expect { create :app, name: "blahonga" }.to raise_error OceanDynamo::RecordNotUnique
    end

    it "should allow an instance to be saved again" do
      app = create :app
      app.save!
      app.save!
      app.save!
    end
  end


  describe "activated_features" do

    before :each do
      @app = create :app, features: {"hotel_flow"=>{ 
                                       "allow_ips"=>"private",
                                       "disallow_user_agents"=>[["IE", "<", 10.0]],
                                       "allow_countries"=>["SE"]
                                    }}
    end

    it "should return a hash" do
      expect(@app.activated_features).to be_a Hash
    end

    it "should take the keyword param :client_ip" do
      expect(@app.activated_features(client_ip: "0.0.0.0")).to eq({"hotel_flow"=>false})
    end

    it "should take the keyword param :username" do
      expect(@app.activated_features(username: "razor")).to eq({"hotel_flow"=>false})
    end

    it "should take the keyword param :ua_name" do
      expect(@app.activated_features(ua_name: "Chrome")).to eq({"hotel_flow"=>false})
    end

    it "should take the keyword param :ua_version" do
      expect(@app.activated_features(ua_version: "99.8")).to eq({"hotel_flow"=>false})
    end

    it "should take the keyword param :ua_os" do
      expect(@app.activated_features(ua_os: "Mac OS 10.9")).to eq({"hotel_flow"=>false})
    end

    it "should allow with correct conditions" do
      allow_any_instance_of(Feature).to receive(:lookup_country).and_return "SE"
      expect(@app.activated_features(client_ip: "10.1.2.3", ua_name: "Chrome", ua_version: "10.1")).
        to eq({"hotel_flow"=>true})
    end

  end

end
