require 'spec_helper'

describe AppsController, :type => :controller do
  
  render_views

  before :each do 
    App.destroy_all
  end

  after :all do 
    App.destroy_all
  end


  describe "DELETE" do
    
    before :each do
      permit_with 200
      @app = create :app
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "so-totally-fake"
    end

    
    it "should return JSON" do
      delete :destroy, id: @app
      expect(response.content_type).to eq "application/json"
    end

    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      delete :destroy, id: @app
      expect(response.status).to eq 400
    end
    
    it "should return a 204 when successful" do
      delete :destroy, id: @app
      expect(response.status).to eq 204
      expect(response.content_type).to eq "application/json"
    end

    it "should return a 404 when the Pnr can't be found" do
      delete :destroy, id: "unheard_of"
      expect(response.status).to eq 404
    end
    
    it "should destroy the Pnr when successful" do
      delete :destroy, id: @app
      expect(response.status).to eq 204
      expect(App.find_by_id(@app.id)).to be_nil
    end
    
  end
  
end
