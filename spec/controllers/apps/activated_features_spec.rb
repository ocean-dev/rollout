require 'spec_helper'

describe AppsController, :type => :controller do
  
  render_views

  before :each do 
    App.destroy_all
  end

  after :all do 
    App.destroy_all
  end


  describe "GET" do
    
    before :each do
      permit_with 200
      @app = create :app, features: {hotels: true, flights: false}
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "totally-fake"
    end

    
    it "should return JSON" do
      get :activated_features, id: @app
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      get :activated_features, id: @app
      expect(response.status).to eq 400
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 404 when the app can't be found" do
      get :activated_features, id: "nescio_nomen"
      expect(response.status).to eq 404
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 200 when successful with a non-resource hash body" do
      get :activated_features, id: @app
      expect(response.status).to eq 200
      expect(response).to render_template(partial: "_app", count: 0)
      expect(JSON.parse(response.body)).to eq({"hotels"=>true, "flights"=>false})
    end
    
    it "should call activated_features with :client_ip, :username, :ua_name, :ua_version, and :ua_os" do
      expect_any_instance_of(App).to receive(:activated_features).
        with({client_ip:"1.2.3.4", username:'razor', ua_name:'IE', ua_version:"8.0", ua_os: "Windows 7"})
      get :activated_features, id: @app, 'client-ip' => '1.2.3.4', 'username' => 'razor',
                                         'ua-name' => 'IE', 'ua-version' => '8.0', 'ua-os' => "Windows 7"
      expect(response.status).to eq 200
    end
  end
end
