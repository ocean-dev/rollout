require 'spec_helper'

describe AppsController, :type => :controller do
  
  render_views

  before :each do 
    App.destroy_all
  end

  after :all do 
    App.destroy_all
  end


  describe "PUT" do
    
    before :each do
      permit_with 200
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "incredibly-fake!"
      @u = create :app
      @args = @u.attributes
    end
     

    it "should return JSON" do
      put :update, @args
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      put :update, @args
      expect(response.status).to eq 400
    end

    it "should return a 404 if the resource can't be found" do
      put :update, id: "keine_Anung"
      expect(response.status).to eq 404
      expect(response.content_type).to eq "application/json"
    end

    it "should return a 422 when resource properties are missing" do
      put :update, id: @u.id
      expect(response.status).to eq 422
      expect(response.content_type).to eq "application/json"
    end

    it "should return a 409 when there is an update conflict" do
      @args['lock_version'] = -2
      put :update, @args
      expect(response.status).to eq 409
    end
        
    it "should return a 200 when successful" do
      put :update, @args
      expect(response.status).to eq 200
      expect(response).to render_template(partial: "_app", count: 1)
    end

    it "should return the updated resource in the body when successful" do
      put :update, @args
      expect(response.status).to eq 200
      expect(JSON.parse(response.body)).to be_a Hash
    end

    #
    # Uncomment this test as soon as there is one or more DB attributes that need
    # validating.
    #
    # it "should return a 422 when there are validation errors" do
    #   put :update, @args.merge('name' => "qz")
    #   response.status.should == 422
    #   response.content_type.should == "application/json"
    #   JSON.parse(response.body).should == {"name"=>["is too short (minimum is 3 characters)"]}
    # end


    it "should alter the app when successful" do
      expect(@u.features).to eq @args['features']
      @args['features'] = {quux: 432}
      put :update, @args
      expect(response.status).to eq 200
      @u.reload
      expect(@u.features).to eq({"quux"=>"432"})
    end

    it "should not allow the name to be changed" do
      expect(@u.name).to eq @args['name']
      @args['name'] = "SomeOtherName"
      put :update, @args
      expect(response.status).to eq 422
      expect(response.status_message).to eq "Unprocessable Entity"
      expect(JSON.parse response.body).to eq({"name"=>["Changing the name of an app is not permitted"]})
      @u.reload
      expect(@u.name).not_to eq "SomeOtherName"
    end

  end
  
end
