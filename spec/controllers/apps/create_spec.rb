require 'spec_helper'

describe AppsController, :type => :controller do
  
  render_views

  before :each do 
    App.destroy_all
  end

  after :all do 
    App.destroy_all
  end


  describe "POST" do
    
   before :each do
      permit_with 200
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "incredibly-fake!"
      @args = build(:app).attributes
    end
    
    
    it "should return JSON" do
      post :create, @args
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      post :create, @args
      expect(response.status).to eq 400
    end
    
    it "should return a 201 when successful" do
      post :create, @args
      expect(response).to render_template(partial: "_app", count: 1)
      expect(response.status).to eq 201
    end

    it "should contain a Location header when successful" do
      post :create, @args
      expect(response.headers['Location']).to be_a String
    end

    it "should return the new resource in the body when successful" do
      post :create, @args.merge("features" => {x: 1})
      expect(JSON.parse(response.body)['app']['features']).to eq({"x"=>"1"})
    end
    
    it "should return a 422 if the App already exists" do
      post :create, @args
      expect(response.status).to eq(201)
      expect(response.content_type).to eq("application/json")
      post :create, @args
      expect(response.status).to eq(422)
      expect(response.content_type).to eq("application/json")
      expect(JSON.parse(response.body)).to eq({"_api_error"=>["Resource not unique"]})
    end

    #
    # Uncomment this test as soon as there is one or more DB attributes that need
    # validating.
    #
    # it "should return a 422 when there are validation errors" do
    #   post :create, @args.merge('name' => "qz")
    #   response.status.should == 422
    #   response.content_type.should == "application/json"
    #   JSON.parse(response.body).should == {"name"=>["is too short (minimum is 3 characters)"]}
    # end
                
  end
  
end
