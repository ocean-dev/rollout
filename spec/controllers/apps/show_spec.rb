require 'spec_helper'

describe AppsController, :type => :controller do
  
  render_views

  before :each do 
    App.destroy_all
  end

  after :all do 
    App.destroy_all
  end


  describe "GET" do
    
    before :each do
      permit_with 200
      @app = create :app
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "totally-fake"
    end

    
    it "should return JSON" do
      get :show, id: @app
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      get :show, id: @app
      expect(response.status).to eq 400
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 404 when the app can't be found" do
      get :show, id: "nescio_nomen"
      expect(response.status).to eq 404
      expect(response.content_type).to eq "application/json"
    end
    
    it "should return a 200 when successful" do
      get :show, id: @app
      expect(response.status).to eq 200
      expect(response).to render_template(partial: "_app", count: 1)
    end
    
  end
  
end
