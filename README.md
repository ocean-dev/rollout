# The Rollout Service

The Rollout Service controls how features intended for all users are gradually
made available at runtime in a controlled, layered fashion.

The Rollout Service is also intended for multivariance testing, including A/B
testing.

It is not intended for enabling or disabling features in applications on a
user-by-user basis.

The toggling is fully dynamic; i.e. does not require the client app to be
re-deployed.


## Installation

First start the common infrastructure, if it isn't already running:
```
  docker-compose -f ../ocean/common.yml up --build
```
Then, to run only the CMS service:
```
  docker-compose -f ../ocean/ocean.yml up --build rollout_service
```
Then run the service setup/update rake task:
```
  docker-compose -f ../ocean/ocean.yml run --rm rollout_service rake ocean:setup_all
```

The `ocean:setup_all` task executes the following rake task:
```
  ocean:update_apps
```
It can be run at any time.


## Running the RSpecs

If you intend to run RSpec tests, start the common infrastructure, if it isn't
already running:
```
  docker-compose -f ../ocean/common.yml up --build
```
Then, to run the actual specs:
```
  docker-compose -f ../ocean/ocean.yml run --rm -e LOG_LEVEL=fatal rollout_service rspec
```
